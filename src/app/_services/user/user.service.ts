import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  userData: any;
  constructor(
    private http: HttpClient,
    private fireAuth: AngularFireAuth,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    this.fireAuth.authState.subscribe((user) => {
      if (user) {
        this.userData = user;
        sessionStorage.setItem('current-user', JSON.stringify(this.userData));
        JSON.parse(sessionStorage.getItem('current-user'));
      } else {
        sessionStorage.setItem('user', null);
        JSON.parse(sessionStorage.getItem('current-user'));
      }
    });
  }

  login(email: string, password: string) {
    return this.fireAuth
      .signInWithEmailAndPassword(email, password)
      .then((value) => {
        console.log('Login worked');
        console.log(value);

        this.router.navigate(['/home']);
        this.snackBar.open(`Login successful with email: ${email}`, 'Ok', { duration: 2000 });
      })
      .catch((err) => {
        console.log('Something went wrong: ', err.message);
      });
  }

  logout() {
    this.fireAuth.signOut();
    sessionStorage.removeItem('auth-token');
    sessionStorage.removeItem('current-user');
    this.router.navigate(['login']);
    this.snackBar.open('Logout successful', 'Ok', { duration: 2000 });
  }

  saveToken(token) {
    sessionStorage.setItem('auth-token', token);
  }
  getToken(): string {
    return sessionStorage.getItem('auth-token');
  }
  saveUser(user) {
    sessionStorage.setItem('current-user', user);
  }
  getUser() {
    return sessionStorage.getItem('current-user');
  }
  isAuthenticated() {
    return sessionStorage.getItem('current-user') ? true : false;
  }

  registerUser(email: string, password: string) {
    this.fireAuth
      .createUserWithEmailAndPassword(email, password)
      .then((value) => {
        console.log('Registration succeeded', value);
      })
      .catch((error) => {
        console.log('Something went wrong: ', error);
      });
  }
}
