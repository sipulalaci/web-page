// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  apiURL: '',
  production: false,
  firebase: {
    apiKey: 'AIzaSyDCIp00bpyNOLrIozoXHyt-Mk_6UhAgwx0',
    authDomain: 'simple-webpage-b6808.firebaseapp.com',
    projectId: 'simple-webpage-b6808',
    storageBucket: 'simple-webpage-b6808.appspot.com',
    messagingSenderId: '8043805420',
    appId: '1:8043805420:web:a3a6a0e41d54b0cd7d03c9',
    measurementId: 'G-70B8PFJEFC',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
